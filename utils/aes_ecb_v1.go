/***************************************************
 ** @Desc : This file for AES/ECB/PKCS5Padding
 ** @Time : 2019.04.09 16:25
 ** @Author : Joker
 ** @File : aes_ecb_v1
 ** @Last Modified by : Joker
 ** @Last Modified time: 2019.04.09 16:25
 ** @Software: GoLand
****************************************************/
package utils

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"encoding/hex"
	"errors"
	"recharge/sys"
	"strings"
)

type AES struct{}

var encrypt = Encrypt{}

//解密
func (the *AES) AesDecrypt(crypted, key []byte) ([]byte, error) {
	upper := strings.ToUpper(encrypt.EncodeMd5([]byte(key)))
	keyEn, _ := hex.DecodeString(upper)

	block, err := aes.NewCipher(keyEn)
	if err != nil {
		sys.LogTrace("AesDecrypt key is error:", err)
		return nil, err
	}
	blockMode := NewECBDecrypter(block)

	cratedByte, _ := encrypt.Base64Decode(string(crypted)) // 转成字节数组
	origData := make([]byte, len(cratedByte))
	blockMode.CryptBlocks(origData, cratedByte)
	origData = the.PKCS5UnPadding(origData)
	return origData, nil
}

//加密
func (the *AES) AesEncrypt(src, key string) (string, error) {
	upper := strings.ToUpper(encrypt.EncodeMd5([]byte(key)))
	keyEn, _ := hex.DecodeString(upper)

	block, err := aes.NewCipher([]byte(keyEn))
	if err != nil {
		sys.LogTrace("AesEncrypt key is error:", err)
		return "", err
	}
	if src == "" {
		sys.LogTrace("AesEncrypt content is empty.")
		return "", errors.New("AesEncrypt content is empty")
	}
	ecb := NewECBEncrypter(block)
	content := []byte(src)
	content = the.PKCS5Padding(content, block.BlockSize())
	crypted := make([]byte, len(content))
	ecb.CryptBlocks(crypted, content)

	t := encrypt.Base64Encode(crypted)
	return t, nil
}

//解密 5.0.0版本
func (the *AES) AesDecryptV2(crypted, key []byte) ([]byte, error) {
	upper := encrypt.EncodeMd5([]byte(key))
	keyEn, _ := hex.DecodeString(upper)

	block, err := aes.NewCipher(keyEn)
	if err != nil {
		sys.LogTrace("AesDecrypt key is error:", err)
		return nil, err
	}
	blockMode := NewECBDecrypter(block)

	cratedByte, _ := encrypt.Base64Decode(string(crypted)) // 转成字节数组
	origData := make([]byte, len(cratedByte))
	blockMode.CryptBlocks(origData, cratedByte)
	origData = the.PKCS5UnPadding(origData)
	return origData, nil
}

//加密 5.0.0版本
func (the *AES) AesEncryptV2(src, key string) (string, error) {
	upper := encrypt.EncodeMd5([]byte(key))
	keyEn, _ := hex.DecodeString(upper)

	block, err := aes.NewCipher([]byte(keyEn))
	if err != nil {
		sys.LogTrace("AesEncrypt key is error:", err)
		return "", err
	}
	if src == "" {
		sys.LogTrace("AesEncrypt content is empty.")
		return "", errors.New("AesEncrypt content is empty")
	}
	ecb := NewECBEncrypter(block)
	content := []byte(src)
	content = the.PKCS5Padding(content, block.BlockSize())
	crypted := make([]byte, len(content))
	ecb.CryptBlocks(crypted, content)

	t := encrypt.Base64Encode(crypted)
	return t, nil
}

func (*AES) PKCS5Padding(ciphertext []byte, blockSize int) []byte {
	padding := blockSize - len(ciphertext)%blockSize //需要padding的数目
	//只要少于256就能放到一个byte中，默认的blockSize=16(即采用16*8=128, AES-128长的密钥)
	//最少填充1个byte，如果原文刚好是blocksize的整数倍，则再填充一个blocksize
	padtext := bytes.Repeat([]byte{byte(padding)}, padding) //生成填充的文本
	return append(ciphertext, padtext...)
}

func (*AES) PKCS5UnPadding(origData []byte) []byte {
	length := len(origData)
	unpadding := int(origData[length-1])
	return origData[:(length - unpadding)]
}

type ecb struct {
	b         cipher.Block
	blockSize int
}

func newECB(b cipher.Block) *ecb {
	return &ecb{
		b:         b,
		blockSize: b.BlockSize(),
	}
}

type ecbEncrypter ecb

// NewECBEncrypter returns a BlockMode which encrypts in electronic code book
// mode, using the given Block.
func NewECBEncrypter(b cipher.Block) cipher.BlockMode {
	return (*ecbEncrypter)(newECB(b))
}
func (x *ecbEncrypter) BlockSize() int { return x.blockSize }
func (x *ecbEncrypter) CryptBlocks(dst, src []byte) {
	if len(src)%x.blockSize != 0 {
		panic("crypto/cipher: input not full blocks")
	}
	if len(dst) < len(src) {
		panic("crypto/cipher: output smaller than input")
	}
	for len(src) > 0 {
		x.b.Encrypt(dst, src[:x.blockSize])
		src = src[x.blockSize:]
		dst = dst[x.blockSize:]
	}
}

type ecbDecrypter ecb

// NewECBDecrypter returns a BlockMode which decrypts in electronic code book
// mode, using the given Block.
func NewECBDecrypter(b cipher.Block) cipher.BlockMode {
	return (*ecbDecrypter)(newECB(b))
}
func (x *ecbDecrypter) BlockSize() int { return x.blockSize }
func (x *ecbDecrypter) CryptBlocks(dst, src []byte) {
	if len(src)%x.blockSize != 0 {
		panic("crypto/cipher: input not full blocks")
	}
	if len(dst) < len(src) {
		panic("crypto/cipher: output smaller than input")
	}
	for len(src) > 0 {
		x.b.Decrypt(dst, src[:x.blockSize])
		src = src[x.blockSize:]
		dst = dst[x.blockSize:]
	}
}
